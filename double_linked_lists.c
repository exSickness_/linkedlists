#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct linkedList{
	double data;
	struct linkedList* next;
};


double ll_sum(struct linkedList* list);
void llist_remove(struct linkedList** list);
void llist_add(struct linkedList** list, double value); // add node as head
// void llist_add(struct linkedList* list, double value);
bool ll_is_sorted(struct linkedList* list, int (*cmp)(double x,double y));
void llist_destroy( struct linkedList* list);
struct linkedList* llist_create(double value);
bool ll_equal_by(struct linkedList* a, struct linkedList* b, int (*cmp)(double x,double y));
int cmp(double x, double y);
bool ll_is_circular(struct linkedList *list);
void ll_reverse(struct linkedList **list);
void ll_insert_sorted(struct linkedList **list, double value);



struct linkedList* llist_create(double value)
{
	struct linkedList* head = malloc( sizeof(*head) );
	if(head) // check for malloc fail
	{
		head->data = value;
		head->next = NULL;
	}
	return(head); // return NULL if malloc fails
}


void llist_destroy( struct linkedList* list)
{
	while(list)
	{
		struct linkedList* tmp = list;
		free(tmp);
		list = list->next;
	}
}

// void llist_add(struct linkedList* list, double value)
void llist_add(struct linkedList** list, double value) // add node as head
{
	struct linkedList *item = llist_create(value);
	if(item)
	{
		item->next = *list;
		*list = item;
	}

	// struct linkedList* item = llist_create(value);
	// item->next = list->next;
	// list->next = item;
}


void llist_remove(struct linkedList** list) // remove first node
{
	struct linkedList *old_head = *list;
	*list = old_head->next;
	old_head->next = NULL;
	llist_destroy(old_head);

	// struct linkedList *new_head = (*list)->next;
	// free(*list);
	// *list = new_head;
}


double ll_sum(struct linkedList* list)
{
	double sum = 0;
	while(list)
	{
		sum += list->data;
		list = list->next;
	}
	return(sum);
}

void ll_print(struct linkedList* list)
{
	printf(">");
	while(list)
	{
		printf("%.2lf,",list->data);
		list = list->next;
	}
	printf("<\n");
}

void ll_append(struct linkedList* a, struct linkedList* b)
{
	while(a->next)
	{
		a = a->next;
	}
	a->next = b;
}

int cmp(double x, double y)
{
	// printf("Comparing %.1lf and %.1lf : ",x,y);
	
	if(x < y)
	{
		// printf("less than\n");
		return(-1);
	}
	else if(x > y)
	{
		// printf("greater than\n");
		return(1);
	}
	else
	{
		// printf("equal to\n");
		return(0);
	}
}

bool ll_equal_by(struct linkedList* a, struct linkedList* b, int (*cmp)(double x,double y))
{
	while( a && b)
	{
		if((*cmp)(a->data, b->data) != 0)
		{
			return(false);
		}
		a = a->next;
		b = b->next;
	}

	return(true);
}

size_t ll_flatten(struct linkedList* list, int (*cmp)(double x,double y))
{
	while(list)// && (list->next != NULL) )
	{
		struct linkedList* tmp = list;
		while(tmp->next != NULL)
		{
			// printf("Address of tmp is: %p\n",(void*)tmp);
			if( (*cmp)( list->data,tmp->next->data)  == 0)
			{
				// remove node
				// list->next = tmp->next;
				tmp->next = tmp->next->next;
			}
			else
			{
				tmp = tmp->next;
			}
		}
		list = list->next;		
	}
	return(75);
}

bool ll_is_sorted(struct linkedList* list, int (*cmp)(double x,double y))
{
	while((list) && (list->next != NULL))
	{
		if((*cmp)(list->data, list->next->data) == 1) // check for cur node bigger than next node
		{
			printf("Unsorted.\n");
			return(false);
		}
		list = list->next;
	}
	return(true);	
}

bool ll_is_circular(struct linkedList *list)
{
	struct linkedList* tmp = list;
	while((list) && (list->next != tmp))
	{
		list = list->next;
	}
	if(list == NULL)
	{
		return(false); // found NULL. not circular
	}
	else
	{
		return(true); // found circular
	}
}

void ll_reverse(struct linkedList **list)
{
	struct linkedList* head = *list;
	struct linkedList* prev = NULL;

	while(head)
	{
		struct linkedList *after = head->next;
		head->next = prev;
		prev = head;
		head = after;
	}
	*list = prev;
}

void ll_insert_sorted(struct linkedList **list, double value)
{
	struct linkedList* head = *list;
	struct linkedList* prev = NULL;
	struct linkedList* created = llist_create(value);

	while((head) && (head->data <= head->next->data)) // loop to ge
	{
		prev = head;
		head = head->next;
	}
	created->next = head;
	if(prev)
	{
		prev->next = created;
	}
	else
	{
		*list = created; // Inserting at the head of the list.	
	}

}

int main(void)
{
	struct linkedList* myList = llist_create(6);

	for(int i = 5; i > 0; i--)
	{
		llist_add(&myList,i);
	}

	struct linkedList* theirList = llist_create(5);

	for(int i = 1; i < 5; i++)
	{
		llist_add(&theirList,i);
	}

	printf("My list: ");
	ll_print(myList);
	printf("Their list: ");
	ll_print(theirList);

	double sum = ll_sum(myList);
	printf("My sum is: %lf!\n",sum);

	//llist_remove(&myList);

	sum = ll_sum(myList);
	printf("My sum is: %lf!\n",sum);

	// ll_append(myList, theirList);

	bool equality = false;
	equality = ll_equal_by(myList, theirList, cmp);

	printf("My list and their list is equal: %d\n",equality);

	struct linkedList* myList2 = llist_create(5);
	llist_add(&myList2,1);
	llist_add(&myList2,1);
	llist_add(&myList2,5);
	llist_add(&myList2,2);
	llist_add(&myList2,2);
	llist_add(&myList2,3);
	llist_add(&myList2,3);
	llist_add(&myList2,2);
	llist_add(&myList2,3);
	llist_add(&myList2,3);
	printf("My list2: ");
	ll_print(myList2);
	size_t lenOfList = ll_flatten(myList2,cmp);
	printf("Length of list2 after flatten: %zd\n",lenOfList);

	printf("My list2: ");
	ll_print(myList2);

	bool isSort = ll_is_sorted(myList,cmp);
	printf("My list is sort: %d\n",isSort);

	bool isCircular = ll_is_circular(myList);
	printf("My list is circular: %d\n",isCircular);
	// ll_append(myList, myList);
	// bool isCircular2 = ll_is_circular(myList);
	// printf("is circular2: %d\n",isCircular2);

	printf("My list before reverse: ");
	ll_print(myList);
	ll_reverse(&myList);
	printf("My list after reverse: ");
	ll_print(myList);


	printf("My list before insert: ");
	ll_print(myList);
	ll_insert_sorted(&myList,72);
	printf("My list after insert: ");
	ll_print(myList);

	llist_destroy(myList2);
	llist_destroy(myList);
	// llist_destroy(theirList);
	return(0);
}